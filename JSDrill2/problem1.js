function stringToEquivalentNumeric(str) {

    if (!str || str.length < 2){
        return 0;
    }
    let newString = "", dotCount = 0;

    for (i = str.length -1; i >= 0; i--){
        if (str[i] === "."){
            newString = str[i] + newString;
            dotCount += 1;
            if (dotCount > 1){
                return 0;
            }

        }
        else if(str[i] >= '0' && str[i] <= '9'){
            newString = str[i] + newString;

        }
        else if(str[i] === ','){
            continue;

        }
        else if (str[i] === '$'){
            if(i == 0){
                return parseFloat(newString);
            }
            else if (i == 1 && (str[0] === '+' || str[0] === '-')){
                newString = str[0] + newString;

            }
            return parseFloat(newString);
        }

        else{
            return 0;
        }
    }
    return 0;
}
const result = stringToEquivalentNumeric('+$200.43');
console.log(result);
const result1 = stringToEquivalentNumeric('-$1000.44');
console.log(result1);

